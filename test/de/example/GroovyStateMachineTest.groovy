package de.example

class GroovyStateMachineTest extends GroovyTestCase {
    private cov19

    protected void setUp() {
        cov19 = GroovyStateMachine.newInstance(State.GESUND)
        def recorder = cov19.record()
        recorder.on(Transition.GESUND).from(State.GESUND).to(State.GESUND)
        recorder.on(Transition.SYMPTOME).from(State.GESUND).to(State.ERKRANKT)
        recorder.on(Transition.NEGATIVER_TEST).from(State.ERKRANKT).to(State.GESUND)
        recorder.on(Transition.POSITIVER_TEST).from(State.ERKRANKT).to(State.INFIZIERT)
        recorder.on(Transition.SCHWACHE_SYMPTOME).from(State.ERKRANKT).to(State.QUARANTAENE)
        recorder.on(Transition.SCHWACHE_SYMPTOME).from(State.KRANKENHAUS).to(State.QUARANTAENE)
        recorder.on(Transition.GENESUNG).from(State.QUARANTAENE).to(State.GESUND)
        recorder.on(Transition.GENESUNG).from(State.KRANKENHAUS).to(State.GESUND)
        recorder.on(Transition.STARKE_SYMPTOME).from(State.INFIZIERT).to(State.KRANKENHAUS)
    }

    void testMoveAlong() {
        cov19.fire(Transition.GESUND)
        cov19.fire(Transition.SYMPTOME)
        cov19.fire(Transition.POSITIVER_TEST)
        cov19.fire(Transition.STARKE_SYMPTOME)
        cov19.fire(Transition.GENESUNG)
        assert(State.GESUND == cov19.currentState)
    }

    void testUndefinedTransition() {
        shouldFail {
            cov19.fire(Transition.POSITIVER_TEST)
        }
    }
}