package de.example

import junit.framework.TestCase.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.jupiter.api.assertThrows

class KotlinStateMachineTest {

    lateinit var stateMachine: StateMachine<State, Transition>

    @Before
    fun setup() {
        stateMachine = StateMachine.withEntry(State.GESUND) {
            (shouldMoveFrom(State.GESUND) to State.GESUND).on(Transition.GESUND)
            (shouldMoveFrom(State.GESUND) to State.ERKRANKT).on(Transition.SYMPTOME)
            (shouldMoveTo(State.GESUND) from State.KRANKENHAUS).on(Transition.GENESUNG)
            (shouldMoveTo(State.GESUND) from State.KRANKENHAUS).on(Transition.GENESUNG)
            (shouldMoveTo(State.INFIZIERT) from State.ERKRANKT).on(Transition.POSITIVER_TEST)
            (shouldMoveTo(State.GESUND) from State.ERKRANKT).on(Transition.NEGATIVER_TEST)
            (shouldMoveFrom(State.INFIZIERT) to State.QUARANTAENE).on(Transition.SCHWACHE_SYMPTOME)
            (shouldMoveFrom(State.INFIZIERT) to State.KRANKENHAUS).on(Transition.STARKE_SYMPTOME)
            (shouldMoveFrom(State.KRANKENHAUS) to State.QUARANTAENE).on(Transition.SCHWACHE_SYMPTOME)
        }
    }

    @Test
    fun moveAlong() {
        stateMachine.evaluate(Transition.GESUND)
        stateMachine.evaluate(Transition.SYMPTOME)
        stateMachine.evaluate(Transition.POSITIVER_TEST)
        stateMachine.evaluate(Transition.STARKE_SYMPTOME)
        stateMachine.evaluate(Transition.GENESUNG);
        assertEquals(State.GESUND, stateMachine.currentState)
    }

    @Test
    fun undefinedTransition() {
        stateMachine.evaluate(Transition.GESUND)
        assertThrows<InvalidTransitionException> { stateMachine.evaluate(Transition.POSITIVER_TEST) }
    }

}
