package de.example

enum State {
    GESUND,
    ERKRANKT,
    INFIZIERT,
    KRANKENHAUS,
    QUARANTAENE
}

enum Transition {
    NEGATIVER_TEST,
    POSITIVER_TEST,
    STARKE_SYMPTOME,
    GENESUNG,
    GESUND,
    SYMPTOME,
    SCHWACHE_SYMPTOME
}

class Grammar {
    def fsm

    def event
    def fromState
    def toState

    Grammar(a_fsm) {
        fsm = a_fsm
    }

    def on(a_event) {
        event = a_event
        this
    }

    def on(a_event, a_transitioner) {
        on(a_event)
        a_transitioner.delegate = this
        a_transitioner.call()
        this
    }

    def from(a_fromState) {
        fromState = a_fromState
        this
    }

    def to(a_toState) {
        assert a_toState, "Invalid toState: $a_toState"
        toState = a_toState
        fsm.registerTransition(this)
        this
    }

    def isValid() {
        event && fromState && toState
    }

    public String toString() {
        "$event: $fromState=>$toState"
    }
}